Exchange Telegram Bot
=========


## Requirements

The following software is required to stand up telegram bot project

|               Prerequisite                       |                 Description              |
|--------------------------------------------------|------------------------------------------|
|[Docker](https://www.docker.com/)                 | Container management tool                |
|[Docker compose](https://docs.docker.com/compose/)| A tool for defining multi-container apps |

## Quick Start

* clone the repo
* cd to the project directory
* insert `BOT_KEY` value to docker-compose.yml
* Run the following:

``` sh
docker-compose build
docker-compose up
```

## Scraper configuration

To adding new scraper use scraper.json example consisted of 6 parameters:
- `url` url to getting information via API
- `platform` short name of exchange platform to rendering bot response
- `xpath` xpath syntax path to price values
- `currency` currency of expected price
- `values_count` number of price values that should be scraped
- `exchange_type` type of exchange operation (`BID` or `ASK`)

## Bot information
To connect the bot
* follow link tg://resolve?domain=Exchange_daily_bot
* send `/start` to get first message from bot

## API examples
To get statics of current period use following parameters:
- `currency`: currency
- `start_datetime`: starting date
- `end_datetime`: ending date
- `platform`: exchange platform (`localbitcoins` or `binance`)
- `exchange_type`: type of exchange operation (`BID` or `ASK`)
You can send request using Python library `requests`
* Run the following:

``` sh
pip install requests
python
```

and run simple python script

```
import requests
import datetime

api_url = 'http://0.0.0.0:8808/exchange/statistics'
end_datetime = datetime.datetime.now()
start_datetime = end_datetime - datetime.timedelta(hours=1)
login = 'foo'
password = 'bar'
json_data = {
    'start_datetime': str(start_datetime),
    'end_datetime': str(end_datetime),
    'currency': 'USD',
    'platform': 'localbitcoins',
    'exchange_type': 'BID'
}
response = requests.get(api_url, params=json_data, auth=(login, password))
```


