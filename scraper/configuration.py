import os

class Config:
    API_URL = 'http://api:8808/exchange'
    CONFIG_PATH = os.path.abspath(
        os.path.join(__file__, '../configs', 'scrapers.json'))
    DELAY = 300
