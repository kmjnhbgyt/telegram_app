import json
import objectpath
import requests
import os
import time
from configuration import Config


class EmptyResultError(BaseException):
    pass


class Scraper:
    def __init__(self, url, xpath, currency, values_count, exchange_type, platform):
        self.url = url
        self.xpath = xpath
        self.currency = currency
        self.values_count = values_count
        self.exchange_type = exchange_type
        self.platform = platform

        api_login = os.getenv('API_LOGIN')
        api_password = os.getenv('API_PASSWORD')

        self.api_auth = (api_login, api_password)

    @staticmethod
    def get_info(url, xpath):
        info = requests.get(url).json()
        items = find_items(info, xpath)
        return items

    def set_db_value(self, price):
        request_json = {'currency': self.currency,
                        'exchange_type': self.exchange_type,
                        'platform': self.platform,
                        'price': price}
        requests.post(Config.API_URL, json=request_json, auth=self.api_auth)

    def scrape(self):
        items = self.get_info(self.url, self.xpath)
        if items:
            for i in items[:self.values_count]:
                self.set_db_value(i)
        else:
            raise EmptyResultError('Cannot found required items using `{self.url}` url')


def create_scrapers():
    file = read_json(Config.CONFIG_PATH)
    scrapers = [Scraper(**config) for config in file]
    return scrapers


def run_scrapers(scrapers):
    while True:
        for i in scrapers:
            i.scrape()
        time.sleep(Config.DELAY)


def read_json(config):
    try:
        with open(config) as conf:
            reader = conf.read()
            return json.loads(reader)
    except Exception:
        raise Exception(f'Invalid file format:{config}')


def find_items(json_config, path):
    try:
        items = objectpath.Tree(json_config).execute(path)
    except SyntaxError as e:
        raise SyntaxError(f'Invalid path `{path}`: {e.msg}')
    if not items:
        raise EmptyResultError(f'Empty result according search with `{path}` path')
    return list(items)


if __name__ == '__main__':
    time.sleep(5)  # hack to synchronize api and scraper
    scrapers = create_scrapers()
    run_scrapers(scrapers)
