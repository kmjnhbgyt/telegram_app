import os
import itertools


class Config:
    API_URL = 'http://api:8808/exchange/statistics'
    BOT_KEY = os.getenv('BOT_KEY')
    API_LOGIN = os.getenv('API_LOGIN')
    API_PASSWORD = os.getenv('API_PASSWORD')
    API_AUTH = (API_LOGIN, API_PASSWORD)
    DELAY = 3600

    MAPPER = {
        'BID': 'Покупка',
        'ASK': 'Продажа',
        'binance': 'https://www.binance.com/',
        'localbitcoins': 'https://localbitcoins.net',
    }
    currencies = ('RUB', 'USD')
    exchange_types = ('BID', 'ASK')
    platforms = ('localbitcoins', 'binance')
    request_items = list(itertools.product(currencies, exchange_types, platforms))
    exchange_pattern = '\t\t{0}\n\t\t\tmin_price: {1}\t\t\tavg_price: {2} \t\t\tmax_price {3}\n'
    platform_pattern = '\t{0}\n'
    currency_pattern = '{0}\n'


def create_response(json):
    """Create text message using pattern"""
    json.sort(key=lambda x: (x['platform'], x['currency']))
    cur_point = None
    cur_platform = None
    res = ''
    for i in json:
        currency = i['currency']
        avg_price = i['avg_price']
        max_price = i['max_price']
        min_price = i['min_price']
        exchange_type = Config.MAPPER[i['exchange_type']]
        platform = Config.MAPPER[i['platform']]
        if cur_point != currency:
            cur_point = currency
            res += Config.currency_pattern.format(currency)
        if cur_platform != platform:
            cur_platform = platform
            res += Config.platform_pattern.format(platform)
        res += Config.exchange_pattern.format(exchange_type, min_price, avg_price, max_price)
    return res
