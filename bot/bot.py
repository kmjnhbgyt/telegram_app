import telebot
import requests
import datetime
import time
from configuration import Config, create_response

bot = telebot.TeleBot(Config.BOT_KEY)


@bot.message_handler(commands=['start'])
def start_message(message):
    """Start messaging every 1 hour"""
    chat_id = message.chat.id

    bot.send_message(message.chat.id, 'Привет, буду отправлять тебе курс раз в час', reply_markup=keyboard1)

    time.sleep(10)

    while True:
        result = []
        for parameters in Config.request_items:
            response = get_statistics(parameters)
            result.append(response)
        message_text = create_response(result)
        bot.send_message(chat_id, message_text, reply_markup=keyboard1)
        time.sleep(Config.DELAY)


def get_statistics(params):
    """Get statistics using API"""
    json = create_request_json(*params)
    response = requests.get(Config.API_URL, params=json, auth=Config.API_AUTH)
    return response.json()


def create_request_json(currency, exchange_type, platform):
    """Create json with parameters for get request"""
    end_datetime = datetime.datetime.now()
    start_datetime = end_datetime - datetime.timedelta(hours=1)
    return {
        'start_datetime': str(start_datetime),
        'end_datetime': str(end_datetime),
        'currency': currency,
        'platform': platform,
        'exchange_type': exchange_type
    }


if __name__ == '__main__':
    keyboard1 = telebot.types.ReplyKeyboardMarkup()
    keyboard1.row('/start')
    bot.polling()
