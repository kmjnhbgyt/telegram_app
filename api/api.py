#!flask/bin/python
from flask import Flask, request
from flask import make_response, jsonify
from flask_httpauth import HTTPBasicAuth
from sqlalchemy import and_
from sqlalchemy.orm import sessionmaker
from model import Exchange, init_db
from configuration import Config
import datetime
from dateutil.parser import parse
import numpy

app = Flask(__name__)

auth = HTTPBasicAuth()


@auth.get_password
def get_password(username):
    if username == Config.LOGIN:
        return Config.PASSWORD
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.route('/exchange/statistics', methods=['GET'])
@auth.login_required
def get_statistics():
    """
    Get minimum, average and maximum price of currency using date range
        Args:
            currency: currency
            start_datetime: starting date
            end_datetime: ending date
            exchange_type: exchange type (BID or ASK)
            platform: exchange platform (localbitcoins or binance)
    """
    start_datetime = request.args.get('start_datetime')
    end_datetime = request.args.get('end_datetime')
    currency = request.args.get('currency')
    exchange_type = request.args.get('exchange_type')
    platform = request.args.get('platform')
    price_list = filter_data(currency=currency,
                             platform=platform,
                             exchange_type=exchange_type,
                             start_datetime=start_datetime,
                             end_datetime=end_datetime)
    avg_price = calculate_avg(price_list)
    max_price = calculate_max(price_list)
    min_price = min(price_list)
    return jsonify({'start_datetime': start_datetime,
                    'end_datetime': end_datetime,
                    'currency': currency,
                    'exchange_type': exchange_type,
                    'platform': platform,
                    'avg_price': avg_price,
                    'max_price': max_price,
                    'min_price': min_price})


@app.route('/exchange', methods=['POST'])
@auth.login_required
def put_data():
    currency = request.json.get('currency')
    exchange_type = request.json.get('exchange_type')
    price = request.json.get('price')
    platform = request.json.get('platform')
    write_to_db(currency=currency,
                exchange_type=exchange_type,
                price=price,
                platform=platform)
    return make_response({'status': 'created'}, 201)


def write_to_db(currency, exchange_type, price, platform):
    """Write data row to db"""
    date = datetime.datetime.now()
    exchange = Exchange(currency=currency,
                        exchange_type=exchange_type,
                        price=price,
                        date=date,
                        platform=platform)
    app.session.add(exchange)
    app.session.commit()


def filter_data(currency, platform, exchange_type, start_datetime, end_datetime):
    """Filter data from db"""
    start = parse(start_datetime, fuzzy=True)
    end = parse(end_datetime, fuzzy=True)
    query = app.session.query(Exchange.price).filter(
        and_(
            Exchange.date.between(start, end),
            Exchange.currency == currency,
            Exchange.exchange_type == exchange_type,
            Exchange.platform == platform
        )
    )
    result_list = [i.price for i in query]
    if not result_list:
        raise Exception(currency,
                        platform,
                        exchange_type,
                        start_datetime,
                        end_datetime)
    return result_list


def calculate_avg(data_list):
    """Calculate average value"""
    filtered = filter_values(data_list)
    avg = sum(filtered) / len(filtered)
    return float(f'{avg:.3f}')

def calculate_max(data_list):
    """Calculate valid max value"""
    filtered = filter_values(data_list)
    return float(f'{max(filtered)}')


def filter_values(data_list):
    """Filter valid values"""
    filtered = [x for x in data_list if numpy.percentile(data_list, 5) <= x <= numpy.percentile(data_list, 95)]
    return filtered


def create_db_session():
    """Create data base session"""
    engine = init_db()
    Session = sessionmaker(bind=engine)
    return Session()


if __name__ == '__main__':
    session = create_db_session()
    app.session = session
    app.run(debug=True, port=Config.PORT, host=Config.HOST)
