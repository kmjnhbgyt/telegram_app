import os


class Config:
    HOST = os.getenv('HOST')
    PORT = os.getenv('PORT')
    DATABASE_URL = os.getenv('DATABASE_URL')
    LOGIN = os.getenv('LOGIN')
    PASSWORD = os.getenv('PASSWORD')
    GUNICORN_BIND = f'{HOST}:{PORT}'
