from gunicorn.app.wsgiapp import WSGIApplication

from api import app, create_db_session
from configuration import Config


class WSGIServer(WSGIApplication):
    def init(self, parser, opts, args):
        pass

    def load(self):
        return app

    def load_config(self):
        self.cfg.set('bind', Config.GUNICORN_BIND)
        super().load_config()


if __name__ == '__main__':
    session = create_db_session()
    app.session = session
    WSGIServer('python wsgi.py [OPTIONS]').run()
