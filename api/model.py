from sqlalchemy import Column, Integer, String, DateTime, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from configuration import Config

Base = declarative_base()

import time

time.sleep(2)


class Exchange(Base):
    __tablename__ = 'exchange'

    id = Column(Integer, primary_key=True, autoincrement=True)
    currency = Column(String(3))
    exchange_type = Column(String(3))
    price = Column(Float)
    date = Column(DateTime, autoincrement=True)
    platform = Column(String(50))

    def __repr__(self):
        return "<Exchenge(currency='%s', exchange_type='%s', price='%s')>" % (
            self.currency, self.exchange_type, self.price)


def init_db():
    engine = create_db_engine(Config.DATABASE_URL)
    Base.metadata.create_all(engine)
    return engine


def create_db_engine(db_url):
    return create_engine(db_url)
