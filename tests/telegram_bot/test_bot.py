import unittest
from bot.configuration import create_response
import ddt


@ddt.ddt
class TestBot(unittest.TestCase):

    @ddt.data(
        ([
             {
                 'currency': 'USD',
                 'exchange_type': 'BID',
                 'platform': 'binance',
                 'avg_price': 2,
                 'max_price': 5,
                 'min_price': 1
             },
             {
                 'currency': 'USD',
                 'exchange_type': 'ASK',
                 'platform': 'binance',
                 'avg_price': 2,
                 'max_price': 5,
                 'min_price': 1
             },
         ],)
    )
    @ddt.unpack
    def test_create_response(self, json):
        actual = create_response(json)
        expected = '''USD
	https://www.binance.com/
		Покупка
			min_price: 1			avg_price: 2 			max_price 5
		Продажа
			min_price: 1			avg_price: 2 			max_price 5
'''
        self.assertEqual(actual, expected)
