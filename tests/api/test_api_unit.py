import unittest
from api import calculate_max, calculate_avg, filter_values
import ddt


@ddt.ddt
class TestAPI(unittest.TestCase):
    @ddt.data(
        ([1, 1, 4, 6, 7, 8, 2, 2, 3, 7, 5, 3, 2000], 8),
        ([1, 1, 4, 6, 7, 2, 2, 3, 7, 5, 3, 100], 7)
    )
    @ddt.unpack
    def test_calculate_max(self, items, expected):
        actual = calculate_max(items)
        self.assertEqual(expected, actual)

    @ddt.data(
        ([10, 11, 11, 12, 12, 2000], 11.5),
        ([1, 20, 30, 25, 25, 100], 25)
    )
    @ddt.unpack
    def test_calculate_avg(self, items, expected):
        actual = calculate_avg(items)
        self.assertEqual(expected, actual)

    @ddt.data(
        (
                [1, 1, 4, 6, 7, 8, 2, 2, 3, 7, 5, 3, 2000],
                [1, 1, 4, 6, 7, 8, 2, 2, 3, 7, 5, 3]
        ),
        (
                [1, 1, 4, 6, 7, 2, 2, 3, 7, 5, 3, 100],
                [1, 1, 4, 6, 7, 2, 2, 3, 7, 5, 3]
        )
    )
    @ddt.unpack
    def test_calculate_avg(self, items, expected):
        actual = filter_values(items)
        self.assertEqual(expected, actual)
