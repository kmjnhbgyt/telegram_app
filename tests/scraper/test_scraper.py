import unittest
import ddt
from scraper import find_items

binance_response = {"symbol": "BTCUSDT",
                    "bidPrice": "6883.69000000",
                    "bidQty": "0.00900000",
                    "askPrice": "6883.70000000",
                    "askQty": "0.18075800"}
localbitcoin_response = {
    "pagination": {
        "next": "https://localbitcoins.net/sell-bitcoins-online/RUB/.json?page=2"
    },
    "data": {
        "ad_list": [
            {
                "data": {
                    "city": "",
                    "location_string": "Russia",
                    "countrycode": "RU",
                    "max_amount": "12000",
                    "lon": 0.0,
                    "require_trade_volume": 0.0,
                    "online_provider": "YANDEXMONEY",
                    "max_amount_available": "12000",
                    "volume_coefficient_btc": "1.50",
                    "bank_name": "",
                    "trade_type": "ONLINE_BUY",
                    "ad_id": 1077406,
                    "temp_price": "520150.00",
                    "payment_window_minutes": 60,
                    "min_amount": "2000",
                    "temp_price_usd": "7082.25",
                    "lat": 0.0,
                    "created_at": "2019-10-03T09:41:33+00:00",
                },
                "actions": {
                    "public_view": "https://localbitcoins.net/ad/1077406"
                }
            },
            {
                "data": {
                    "countrycode": "RU",
                    "max_amount": "28400",
                    "lon": 0.0,
                    "require_trade_volume": 0.0,
                    "online_provider": "SPECIFIC_BANK",
                    "max_amount_available": "28400",
                    "volume_coefficient_btc": "1.50",
                    "trade_type": "ONLINE_BUY",
                    "ad_id": 287002,
                    "temp_price": "520050.66",
                    "payment_window_minutes": 60,
                    "min_amount": "14200",
                    "limit_to_fiat_amounts": "",
                    "temp_price_usd": "7080.90"
                },
                "actions": {
                    "public_view": "https://localbitcoins.net/ad/287002"
                }
            },
            {
                "data": {
                    "city": "",
                    "location_string": "Russia",
                    "countrycode": "RU",
                    "max_amount": "9105",
                    "lon": 0.0,
                    "require_trade_volume": 0.0,
                    "online_provider": "QIWI",
                    "max_amount_available": "9105",
                    "volume_coefficient_btc": "1.50",
                    "trade_type": "ONLINE_BUY",
                    "ad_id": 1063358,
                    "temp_price": "520007.77",
                    "temp_price_usd": "7080.31",
                    "lat": 0.0,
                    "created_at": "2019-09-07T08:46:14+00:00",
                },
                "actions": {
                    "public_view": "https://localbitcoins.net/ad/1063358"
                }
            }
        ],
        "ad_count": 50
    }
}


@ddt.ddt
class TestScraper(unittest.TestCase):

    @ddt.data(
        (binance_response, "$..bidPrice", ["6883.69000000"],),
        (localbitcoin_response, "$..temp_price", ["520150.00", "520050.66", "520007.77"],),
    )
    @ddt.unpack
    def test_find_items(self, json, xpath, items):
        result = find_items(json, xpath)
        self.assertEquals(result, items)

    @ddt.data(
        (binance_response, "..bidPrice", SyntaxError,),
    )
    @ddt.unpack
    def test_find_items(self, json, xpath, error):
        with self.assertRaises(error):
            find_items(json, xpath)
